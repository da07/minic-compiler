%{
#include<stdio.h>
#include<stdlib.h>
#define YYSTYPE int
int yyparse();
int yylex();
int yyerror(char *s);
extern FILE* yyin;

%}
//Symboles terminaux qui seront fournis par yylex()
%token IDENT FMAIN DEFINE
%token ENTIER CHAINE
%token VIRG POINTVIRG PAROUV PARFER ACCOUV ACCFER
%token FDF
%token SI SINON TANTQUE
%token AFFECT LIRE ECRIRE
%token INF INFEGAL DIFF SUP SUPEGAL EGAL
%left PLUS MOINS
%right 	MULT DIVI
%start FICHIER

%%
FICHIER : PROGRAMME 

PROGRAMME : DECL_CONST PROGRAMME2| DECL_VAR PROG| PROG
PROGRAMME2 : DECL_VAR| PROG

DECL_CONST : DEFINE IDENT ENTIER DECL_CONST2

DECL_CONST2 : /*empty*/

DECL_VAR : ENTIER IDENT DECL_VAR2

DECL_VAR2 : SUITE_VAR POINTVIRG DECL_VAR3| POINTVIRG DECL_VAR

DECL_VAR3 : DECL_VAR| /*empty*/

SUITE_VAR : VIRG IDENT SUITE_VAR2

SUITE_VAR2 : SUITE_VAR| /*empty*/

PROG : FMAIN PAROUV PARFER BLOC

BLOC : ACCOUV BLOC2

BLOC2 : AUTRES_INST ACCFER

AUTRES_INST : INSTRUCTION AUTRES_INST2

AUTRES_INST2 : AUTRES_INST| /*empty*/

INSTRUCTION :  ITERATION| AFFECTATION| LECTURE| ECRITURE| CONDITIONNELLE

CONDITIONNELLE : SI PAROUV EXP PARFER BLOC SUITE_COND

SUITE_COND : /*empty*/| SINON BLOC

ITERATION : TANTQUE PAROUV EXP PARFER BLOC

AFFECTATION : IDENT AFFECT EXP POINTVIRG

LECTURE : LIRE PAROUV IDENT PARFER POINTVIRG

ECRITURE : ECRIRE PAROUV ECRITURE2

ECRITURE2 : PARFER POINTVIRG| EXP_OU_CH ECRITURE3

ECRITURE3 : AUTRES_ECRI PARFER POINTVIRG| PARFER POINTVIRG

AUTRES_ECRI : VIRG EXP_OU_CH AUTRES_ECRI2

AUTRES_ECRI2 : AUTRES_ECRI| /*empty*/

EXP_OU_CH : EXP| CHAINE

EXP : TERME EXP2 

EXP2 : OP_BIN EXP| OP_REL EXP|/*empty*/

TERME : ENTIER| IDENT| PAROUV EXP PARFER| MOINS TERME
OP_BIN : PLUS| MOINS| MULT| DIVI 
OP_REL : INF| INFEGAL| DIFF| SUPEGAL |SUP

%%

int yyerror(char *s) {
	printf("%s\n", s );
	return 0;
}
int main(int argc, char *argv[])
{
	yyin = fopen(argv[1], "r");
	
   if(!yyparse())
		printf("\nParSIng complete\n");
	else
		printf("\nParSIng failed\n");
	
	fclose(yyin);
    return 0;
}
         
    

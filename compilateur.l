%{
#include <stdlib.h>
#include <stdio.h>
#include "compilateur.tab.h" 
%}
lettre 		[a-zA-Z]
chaine		[A-Za-z]
chiffre 	[0-9]
entier		[0-9]+
id 		{lettre}({lettre}|{chiffre})*
affect		\=
plus		\+
moins		\-
multi		\*
divi		\/
virg		\,
pointvirg	\;

inf		\<
infegal		\=<
diff		\!=
sup		\>

parouv		\(
parfer		\)
accouv		\{
accfer		\}

%%
"main"		{return(FMAIN);}
"#define"	{return(DEFINE);}
"scanf"		{return(LIRE);}
"printf" 	{return(ECRIRE);}
"while" 	{return(TANTQUE);}
"if"		{return(SI);}
"else"		{return(SINON);}
<<EOF>>		{return(0);}

{chaine}	{return(CHAINE);}
{entier}	{ return(ENTIER); }
{affect}	{ return(AFFECT); }
{plus}		{return(PLUS);}
{moins}		{return(MOINS);}
{multi}		{return(MULT);}
{divi}		{return(DIVI);}
{virg}		{ return(VIRG); }
{pointvirg}	{return(POINTVIRG);}
"=="		{return(EGAL);}
{inf}		{ return(INF); }
{infegal}	{ return(INFEGAL); }
{diff}		{return(DIFF);}
{sup}		{return(SUP);}
">="		{return(SUPEGAL);}
{parouv}	{ return(PAROUV); }
{parfer}	{ return(PARFER); }
{accouv}	{ return(ACCOUV); }
{accfer}	{ return(ACCFER); }



{id} 		{ return(IDENT); }
\n 		{++yylineno;}
%%










 
 
